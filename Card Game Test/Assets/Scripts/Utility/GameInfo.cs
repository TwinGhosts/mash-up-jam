﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfo : MonoBehaviour
{
    [Header("Card Rotation")]
    public DragRotatorInfo rotatorInfo;
    public DragRotatorAxisInfo axisInfo;

    [Header("Card Info")]
    public float cardBreakDistance = 2f;
    public float cardResetTime = 2f;

    public static GameInfo Instance;

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
}
