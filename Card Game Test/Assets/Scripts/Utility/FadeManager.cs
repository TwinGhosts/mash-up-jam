﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FadeManager : MonoBehaviour
{
    [SerializeField]
    private Graphic _fadeGraphic = null;
    private Color _color;
    private bool _isFading = false;

    public UnityEvent OnFadeOut;
    public UnityEvent OnFadeIn;

    private void Awake ()
    {
        Util.FadeManager = this;
        _fadeGraphic.gameObject.SetActive(true);
    }

    // Use this for initialization
    private void Start ()
    {
        _color = _fadeGraphic.color;
        Fade(true);
    }

    public void Fade (bool fadeIn)
    {
        if (!_isFading)
            StartCoroutine(_Fade(fadeIn, 1f));
    }

    public void Fade (bool fadeIn, float duration = 0.4f)
    {
        if (!_isFading)
            StartCoroutine(_Fade(fadeIn, duration));
    }

    public void RestartScene (float duration = 0.4f)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex));
        }
    }

    public void NextScene (float duration = 0.4f)
    {
        if (!_isFading)
        {
            StartCoroutine(_Fade(false, duration));
            OnFadeOut.AddListener(() => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1));
        }
    }

    private IEnumerator _Fade (bool fadeIn, float duration)
    {
        _isFading = true;

        var progress = 0f;
        var startColor = _fadeGraphic.color;
        var endColor = _color;
        endColor.a = (fadeIn) ? 0f : 1f;

        while (progress < 1f)
        {
            progress += Time.deltaTime / duration;
            _fadeGraphic.color = Color.Lerp(startColor, endColor, progress);
            yield return null;
        }

        _isFading = false;
        if (fadeIn)
            OnFadeIn.Invoke();
        else
            OnFadeOut.Invoke();
    }
}
