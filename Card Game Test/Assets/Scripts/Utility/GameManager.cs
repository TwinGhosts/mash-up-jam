﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [Header("Regular Variables")]
    public int currentTurn = 0;
    public Transform worldCanvas;
    public float tileSize = 1f;
    [HideInInspector]
    public float tileSizeHalf;

    [Header("Objectives")]
    public List<Coin> coins = new List<Coin>();

    [HideInInspector]
    public AstarPath aStar;

    [Header("Cards")]
    public int amountOfCardsPerTurn = 6;
    public int maxCardsPlayed = 2;

    [Header("Card Lists")]
    [HideInInspector]
    public List<Card> currentCards = new List<Card>();
    public List<CardMovement> movementCards = new List<CardMovement>();
    public List<CardRotation> rotationCards = new List<CardRotation>();

    [HideInInspector]
    public List<EntityGhost> ghosts = new List<EntityGhost>();
    public int ghostPowerUpTurns = 0;

    [Header("UI Stuff")]
    public Text scoreText;
    public Text timeText;
    public Text coinAmountText;
    public static int score;
    public static float time;

    private float seconds;
    private float minutes;
    private float hours;

    [HideInInspector]
    public int cardsPlayed = 0;
    public List<Transform> spawnLocations = new List<Transform>();

    private void Awake()
    {
        Util.GameManager = this;
        tileSizeHalf = tileSize / 2f;
    }

    private void Start()
    {
        aStar = FindObjectOfType<AstarPath>();
        FillCardDeck();

        scoreText.text = "" + score;
        timeText.text = "" + Mathf.RoundToInt(time);
        coinAmountText.text = "" + coins.Count;
    }

    private void Update()
    {
        time += Time.deltaTime;

        hours = Mathf.FloorToInt(time / 60f / 60f);
        minutes = Mathf.FloorToInt(time / 60f);
        seconds = Mathf.FloorToInt(time - minutes * 60);

        if (float.IsNaN(hours)) hours = 0;
        if (float.IsNaN(minutes)) minutes = 0;

        timeText.text = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);
    }

    private void FillCardDeck()
    {
        // Remove the current hand
        for (int i = currentCards.Count - 1; i >= 0; i--)
        {
            var card = currentCards[i];
            if (card)
                Destroy(card.gameObject);
        }
        currentCards.Clear();

        // Calculate the screen width and height to place the cards
        var height = Camera.main.orthographicSize * 2f;
        var width = height * Camera.main.aspect;
        var camera = Camera.main.transform.position;

        var spawnDepth = -2f;

        for (int i = 0; i < amountOfCardsPerTurn; i++)
        {
            var card = Instantiate(GetRandomCard(), worldCanvas);
            var spawnPosition = Vector3.zero;
            spawnPosition.x = camera.x - width / 4f + (((width / 2f) / amountOfCardsPerTurn) * i) + (card.transform.localScale.x / 2f);
            spawnPosition.y = camera.y - height / 2f + 2f;
            spawnPosition.z = spawnDepth;

            card.transform.position = spawnPosition;
            currentCards.Add(card);
        }
    }

    public void UpdateScore()
    {
        scoreText.text = "" + score;
    }

    public void PlayCard()
    {
        cardsPlayed = (int)Mathf.Repeat(cardsPlayed + 1, maxCardsPlayed);

        if (cardsPlayed == 0)
        {
            EndTurn();
        }
    }

    public void ActivateGhostPowerup(int turns)
    {
        ghostPowerUpTurns = turns;
        foreach (var ghost in ghosts)
        {
            ghost.PowerupSwitch();
        }
    }

    public bool CollectedAllCoins { get { return coins.Count == 0; } }

    public Card GetRandomCard()
    {
        return (Random.Range(0, 2) == 0) ? GetRandomMovementCard() : GetRandomRotationCard();
    }

    public Card GetRandomMovementCard()
    {
        return movementCards[(int)Random.Range(0, movementCards.Count)];
    }

    public Card GetRandomRotationCard()
    {
        return rotationCards[(int)Random.Range(0, rotationCards.Count)];
    }

    public void CollectCoin(Coin coin)
    {
        coins.Remove(coin);

        score += 15;
        scoreText.text = "" + score;
        coinAmountText.text = "" + coins.Count;

        Destroy(coin.gameObject);

        if (CollectedAllCoins)
        {
            Util.FadeManager.Fade(false);
            Util.FadeManager.OnFadeOut.AddListener(() => Util.FadeManager.NextScene());
        }
    }

    public void EndTurn()
    {
        currentTurn = (int)Mathf.Repeat(currentTurn + 1, 2);

        if (currentTurn != 0) StartCoroutine(Util.AI.UseTurn());
        else
        {
            ghostPowerUpTurns--;
            if (ghostPowerUpTurns <= 0)
            {
                foreach (var ghost in ghosts)
                {
                    ghost.ResetPowerup();
                }
            }
            FillCardDeck();
        }
    }
}
