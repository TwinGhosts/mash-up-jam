﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Util
{
    public static GameManager GameManager;
    public static FadeManager FadeManager;
    public static EntityPlayer Player;
    public static AI AI;
    public static CameraBehaviour CameraBehaviour;
    public static Compass Compass;

    public static List<EntityBase> Units = new List<EntityBase>();

    public static EntityBase UnitAtPosition(Vector2Int position)
    {
        // Check enemy units
        foreach (var unit in Units)
        {
            if (unit.GetGridPosition() == position)
            {
                return unit;
            }
        }

        return null;
    }
}
