﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Blink : MonoBehaviour
{
    public float blinkTime = 0.75f;
    public string sceneToLoad = "Main Menu";
    public Text textToBlink;

    private float time = 0f;

    private void Update()
    {
        time += Time.deltaTime;
        if (time >= blinkTime)
        {
            textToBlink.enabled = !textToBlink.enabled;
            time = 0f;
        }

        if (Input.anyKeyDown)
        {
            Util.FadeManager.Fade(false);
            Util.FadeManager.OnFadeOut.AddListener(() => SceneManager.LoadScene(sceneToLoad));
        }
    }
}
