﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuParallax : MonoBehaviour
{
    public Transform layerSkybox;
    public Transform layerBackground;
    public Transform layerForeground;

    private void LateUpdate()
    {
        var mouseDistanceFromCenter = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        layerSkybox.position = mouseDistanceFromCenter * 0.02f;
        layerBackground.position = mouseDistanceFromCenter * 0.03f;
        layerForeground.position = mouseDistanceFromCenter * 0.05f;
    }
}
