﻿using UnityEngine;
using System.Collections;

public class EntityGhost : EntityBase
{
    public bool canHit = true;
    public bool canDie = false;

    public bool isDead = false;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        Util.GameManager.ghosts.Add(this);
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Die()
    {
        isDead = true;
        StartCoroutine(_Respawn());
    }

    protected IEnumerator _Respawn()
    {
        var progress = 0f;
        var startPosition = transform.position;
        var endPosition = FindFurthestSpawnLocation();
        var color = Color.blue;
        color.a = 0.3f;

        spriteRenderer.color = color;

        while (progress < 1f)
        {
            progress += Time.deltaTime / 1.5f;
            transform.position = Vector2.Lerp(startPosition, endPosition, Mathf.SmoothStep(0f, 1f, progress));
            yield return null;
        }

        color.a = 1f;
        spriteRenderer.color = color;

        isDead = false;
    }

    protected Vector2 FindFurthestSpawnLocation()
    {
        float furthestDistance = 0f;
        Transform furthestSpawnPoint = null;
        foreach (var spawnPoint in Util.GameManager.spawnLocations)
        {
            var distance = (spawnPoint.position - transform.position).magnitude;
            if (distance > furthestDistance)
            {
                furthestDistance = distance;
                furthestSpawnPoint = spawnPoint;
            }
        }

        return furthestSpawnPoint.position;
    }

    public void PowerupSwitch()
    {
        GetComponentInChildren<SpriteRenderer>().color = Color.blue;
        canHit = false;
        canDie = true;
    }

    public void ResetPowerup()
    {
        GetComponentInChildren<SpriteRenderer>().color = Color.white;
        canDie = false;
        canHit = true;
    }
}
