﻿using UnityEngine;
using System.Collections;

public class EntityPlayer : EntityBase
{
    public Transform movementArrow;

    protected override void Awake()
    {
        Util.Player = this;
        base.Awake();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Die()
    {
        Util.CameraBehaviour.TriggerShake(0.2f);
        Util.FadeManager.Fade(false, 0.2f);
        spriteRenderer.enabled = false;
        Util.FadeManager.OnFadeOut.AddListener(() => Util.FadeManager.RestartScene());
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        // Hit enemy
        var ghost = collision.GetComponent<EntityGhost>();
        if (ghost && !ghost.isDead)
        {
            if (ghost.canHit)
                TakeDamage(1);
            if (ghost.canDie)
                ghost.Die();
        }

        // Touch teleporter
        var teleporter = collision.GetComponent<Teleporter>();
        if (teleporter && teleporter.endPoint)
        {
            teleporter.Teleport(this);
        }
    }

    protected override void SetRotation()
    {
        base.SetRotation();

        Util.Compass.SetDirection(direction);

        switch (direction)
        {
            default:
            case 0:
                movementArrow.position = (Vector2)transform.position + new Vector2(0f, 0.625f);
                movementArrow.rotation = Quaternion.Euler(0f, 0f, 0f);
                break;
            case 1:
                movementArrow.position = (Vector2)transform.position + new Vector2(0.625f, 0f);
                movementArrow.rotation = Quaternion.Euler(0f, 0f, -90f);
                break;
            case 2:
                movementArrow.position = (Vector2)transform.position + new Vector2(0f, -0.625f);
                movementArrow.rotation = Quaternion.Euler(0f, 0f, 180f);
                break;
            case 3:
                movementArrow.position = (Vector2)transform.position + new Vector2(-0.625f, 0f);
                movementArrow.rotation = Quaternion.Euler(0f, 0f, -270f);
                break;
        }
    }
}
