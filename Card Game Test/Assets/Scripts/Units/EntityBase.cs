﻿using UnityEngine;
using Pathfinding;

[SelectionBase]
[RequireComponent(typeof(Seeker), typeof(EntityMovementAI), typeof(BoxCollider2D))]
public class EntityBase : MonoBehaviour
{
    protected SpriteRenderer spriteRenderer;
    protected Vector2 spriteScale;
    protected Vector2 startingSpriteScale;
    protected const float inflationRate = 0.02f;
    protected float tileSizeHalf;

    public AnimationClip idleAnimation;
    public AnimationClip movingAnimation;

    [HideInInspector]
    public EntityMovementAI pathfinder;
    public int direction = 0;
    public int health = 1;
    [HideInInspector]
    public int maxHealth;
    [HideInInspector]
    public Vector2Int previousPosition;
    // 0 = up
    // 1 = right
    // 2 = down
    // 3 = left

    [HideInInspector]
    public bool isMoving = false;

    protected virtual void Awake()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        pathfinder = GetComponent<EntityMovementAI>();

        spriteScale = spriteRenderer.gameObject.transform.localScale;
        startingSpriteScale = spriteScale;
        previousPosition = new Vector2Int((int)transform.position.x, (int)transform.position.y);
    }

    protected virtual void Start()
    {
        tileSizeHalf = Util.GameManager.tileSizeHalf;
        transform.position = new Vector2(GetGridPosition().x + tileSizeHalf, GetGridPosition().y + tileSizeHalf);
        SetRotation();
    }

    protected virtual void Update()
    {
        // Fix size
        spriteScale.x = MathEx.Approach(spriteScale.x, startingSpriteScale.x, inflationRate);
        spriteScale.y = MathEx.Approach(spriteScale.y, startingSpriteScale.y, inflationRate);
        spriteRenderer.transform.localScale = spriteScale;

        // Sort based on height
        spriteRenderer.sortingOrder = Mathf.CeilToInt(-transform.position.y + 300);
    }

    public virtual void Die()
    {
        Destroy(gameObject);
    }

    public virtual bool Move(Vector2 newPosition, int amount)
    {
        var actualPosition = newPosition + GetForwardDirection() * amount;
        pathfinder.OnPathCompletion.AddListener(() => GetComponentInChildren<Animator>().Play(idleAnimation.name));
        pathfinder.OnPathCompletion.AddListener(() => isMoving = false);
        pathfinder.OnPathCompletion.AddListener(() => spriteScale = startingSpriteScale * 0.8f);
        pathfinder.Seek(actualPosition, amount);

        GetComponentInChildren<Animator>().Play(movingAnimation.name);

        return true;
    }

    public void TakeDamage(int amount)
    {
        health = (int)Mathf.Repeat(health - amount, maxHealth);
        if (health <= 0)
        {
            Die();
        }
    }

    public virtual bool MoveForward(int amount)
    {
        return Move(GetGridPosition() + GetForwardDirection(), amount);
    }

    public virtual bool MoveLeft(int amount)
    {
        return Move(GetGridPosition() + Vector2Int.left, amount);
    }

    public virtual bool MoveRight(int amount)
    {
        return Move(GetGridPosition() + Vector2Int.right, amount);
    }

    public virtual bool MoveUp(int amount)
    {
        return Move(GetGridPosition() + Vector2Int.up, amount);
    }

    public virtual bool MoveDown(int amount)
    {
        return Move(GetGridPosition() + Vector2Int.down, amount);
    }

    public Vector2Int GetGridPosition()
    {
        return new Vector2Int(Mathf.FloorToInt(transform.position.x), Mathf.FloorToInt(transform.position.y));
    }

    public Vector2Int GetForwardDirection()
    {
        switch (direction)
        {
            default:
            case 0:
                return Vector2Int.up;
            case 1:
                return Vector2Int.right;
            case 2:
                return Vector2Int.down;
            case 3:
                return Vector2Int.left;
        }
    }

    public void Rotate(int amount)
    {
        direction = (int)Mathf.Repeat(direction + amount, 4);
        SetRotation();
    }

    protected virtual void SetRotation()
    {
    }
}