﻿using UnityEngine;
using Pathfinding;
using UnityEngine.Events;
using System.Collections.Generic;

public class EntityMovementAI : MonoBehaviour
{
    public Path path;
    public float visualMovementSpeed = 15f;
    [HideInInspector]
    public float nextNodeDistance = 0.00001f;
    [HideInInspector]
    public bool isMoving = false;

    private float nodesTraveled = 0;
    private float maxNodesTraveled = 0;

    [HideInInspector]
    public Seeker seeker;
    private int currentNode = 0;

    [HideInInspector]
    public UnityEvent OnPathCompletion = new UnityEvent();

    public void Awake()
    {
        seeker = GetComponent<Seeker>();
        OnPathCompletion.AddListener(() => isMoving = false);
        OnPathCompletion.AddListener(() => nodesTraveled = 0);
    }

    public void Seek(Vector2 targetLocation, int maxNodesTraveled)
    {
        if (!(targetLocation.x % 1 > 0)) targetLocation.x += 0.5f;
        if (!(targetLocation.y % 1 > 0)) targetLocation.y += 0.5f;

        // Start finding a part to the new target location
        this.maxNodesTraveled = maxNodesTraveled;
        seeker.StartPath(transform.position, targetLocation, OnPathComplete);
        isMoving = true;
    }

    private void OnPathComplete(Path p)
    {
        // Reset on finish
        if (!p.error)
        {
            path = p;
            currentNode = 0;
        }
    }

    public void Reset()
    {
        nodesTraveled = 0;
    }

    public void FixedUpdate()
    {
        // Fail conditions
        if (path == null) { return; }

        if (currentNode >= path.path.Count && isMoving)
        {
            OnPathCompletion.Invoke();
            return;
        }
        if (currentNode >= path.path.Count) { return; }
        if (nodesTraveled >= maxNodesTraveled + 1 && currentNode != path.path.Count)
        {
            currentNode = path.path.Count;
            OnPathCompletion.Invoke();
            return;
        }

        // Get the direction towards the next waypoint
        var nextNode = path.path[currentNode].position;
        var nextNodePosition = (Vector3)nextNode;

        // Move
        gameObject.transform.position = Vector3.MoveTowards(transform.position, nextNodePosition, visualMovementSpeed * Time.fixedDeltaTime);

        // Go to the next node
        if (Vector3.Distance(transform.position, nextNodePosition) < nextNodeDistance)
        {
            nodesTraveled++;
            currentNode++;
            // Music.Instance.PlaySound(Music.Instance.s_effects, Music.Instance.e_move, 0.085f);
            return;
        }
    }
}
