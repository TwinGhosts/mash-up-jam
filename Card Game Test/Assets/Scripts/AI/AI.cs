﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour
{
    private GameManager gm;
    [HideInInspector]
    public List<EntityGhost> actors = new List<EntityGhost>();

    private void Awake()
    {
        Util.AI = this;
    }

    private void Start()
    {
        gm = Util.GameManager;
    }

    public IEnumerator UseTurn()
    {
        actors = gm.ghosts;

        foreach (var actor in actors)
        {
            if (actor.canDie)
                actor.Move(Util.Player.transform.position, 1);
            else
                actor.Move((transform.position - Util.Player.transform.position).normalized + transform.position, 1);

            yield return new WaitForSeconds(0.25f);
        }

        yield return new WaitForSeconds(1f);
        gm.EndTurn();
    }
}
