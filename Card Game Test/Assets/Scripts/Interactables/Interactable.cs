﻿using UnityEngine;
using System.Collections;

public class Interactable : MonoBehaviour
{
    protected float animationProgress = 0f;
    protected bool moveDown = false;
    protected float animationHeightFromStartPosition = 0.15f;
    protected Vector3 animationHeightPosition;
    protected Vector3 startPosition;

    // Use this for initialization
    protected void Awake()
    {
        startPosition = transform.position;
        animationHeightPosition = new Vector3(transform.position.x, transform.position.y + animationHeightFromStartPosition, transform.position.z);
        moveDown = (Random.Range(0f, 1f) < 0.5f) ? true : false;
        animationProgress = Random.Range(0f, 1f);
    }

    // Update is called once per frame
    protected void LateUpdate()
    {
        animationProgress += Time.deltaTime / 2f;

        if (animationProgress >= 1f)
        {
            moveDown = !moveDown;
            animationProgress = 0f;
        }

        transform.position = (moveDown) ? Vector3.Lerp(startPosition, animationHeightPosition, Mathf.SmoothStep(0f, 1f, animationProgress))
            : Vector3.Lerp(animationHeightPosition, startPosition, Mathf.SmoothStep(0f, 1f, animationProgress));
    }
}
