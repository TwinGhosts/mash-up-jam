﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour
{
    public Transform endPoint;
    [HideInInspector]
    public bool isTeleporting = false;

    public void Teleport(EntityBase teleportTarget)
    {
        if (!isTeleporting)
        {
            StartCoroutine(_Teleport(teleportTarget));
        }
    }

    private IEnumerator _Teleport(EntityBase teleportTarget)
    {
        var progress = 0f;
        var startColor = teleportTarget.GetComponentInChildren<SpriteRenderer>().color;
        var endColor = startColor;
        endColor.a = 0f;
        isTeleporting = true;

        while (progress < 1f)
        {
            progress += Time.deltaTime / 1f;
            teleportTarget.GetComponentInChildren<SpriteRenderer>().color = Color.Lerp(startColor, endColor, Mathf.SmoothStep(0, 1, progress));
            yield return null;
        }

        teleportTarget.transform.position = endPoint.position;
        yield return new WaitForSeconds(1f);

        progress = 0f;
        while (progress < 1f)
        {
            progress += Time.deltaTime / 1f;
            teleportTarget.GetComponentInChildren<SpriteRenderer>().color = Color.Lerp(endColor, startColor, Mathf.SmoothStep(0, 1, progress));
            yield return null;
        }
        isTeleporting = false;
    }

    private void OnDrawGizmos()
    {
        if (endPoint)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, endPoint.position);
        }
    }
}
