﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostInversion : Interactable
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<EntityPlayer>())
        {
            Util.GameManager.ActivateGhostPowerup(6);
            GameManager.score += 135;
            Util.GameManager.UpdateScore();
            Destroy(gameObject);
        }
    }
}
