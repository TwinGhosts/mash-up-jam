﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Interactable
{
    private void Start()
    {
        Util.GameManager.coins.Add(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<EntityPlayer>())
        {
            Util.GameManager.CollectCoin(this);
        }
    }
}
