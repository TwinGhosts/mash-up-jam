﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour
{
    [Header("Effects UI")]
    public AudioClip e_confirm;
    public AudioClip e_move;
    public AudioClip e_hit;

    [Header("Music")]
    public AudioClip m_mainMenu;
    public AudioClip m_game;

    [Header("Sources")]
    public AudioSource s_music;
    public AudioSource s_effects;

    public static Music Instance;

    void Awake()
    {
        if (!Instance)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            SceneManager.LoadScene("Main Menu");
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene("Game 1");
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene("Game 2");
        }

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SceneManager.LoadScene("Game 3");
        }

        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            SceneManager.LoadScene("Credits");
        }


        if (SceneManager.GetActiveScene().name == "Main Menu" || SceneManager.GetActiveScene().name == "Credits" || SceneManager.GetActiveScene().name == "Controls")
        {
            if (s_music.clip != m_mainMenu)
            {
                StopMusic();
                PlayMusic(m_mainMenu, 0.4f, true);
            }
        }
        else
        {
            if (s_music.clip != m_game)
            {
                StopMusic();
                PlayMusic(m_game, 0.1f, true);
            }
        }
    }

    public void PlaySound(AudioSource source, AudioClip clipToPlay, float volume)
    {
        source.PlayOneShot(clipToPlay, volume);
    }

    public void PlayMusic(AudioClip musicClip, float volume, bool repeat)
    {
        s_music.clip = musicClip;
        s_music.volume = volume;
        s_music.Play();
        s_music.loop = repeat;
    }

    public void StopMusic()
    {
        s_music.Stop();
    }
}
