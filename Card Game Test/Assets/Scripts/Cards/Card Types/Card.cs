﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Card : MonoBehaviour
{
    protected Vector3 startPosition;
    protected Vector3 mousepos;
    protected Vector3 clickedPos;
    protected Vector3 offSet;
    protected bool isSelected = false;
    protected float resetTimer = 0f;
    protected ParticleSystem ps;

    [HideInInspector]
    public Vector2 startingScale;
    [HideInInspector]
    public Vector2 targetScale;
    [HideInInspector]
    public Vector2 scale;
    [HideInInspector]
    public float growSpeedX = 0.1f;
    [HideInInspector]
    public float growSpeedY = 0.15f;

    public UnityEvent OnActivate = new UnityEvent();

    public bool CanSelect { get { return Util.GameManager.currentTurn == 0; } }

    protected void Start()
    {
        startPosition = transform.position;
        startingScale = transform.localScale;
        targetScale = transform.localScale;
        transform.localScale = Vector2.zero;
        scale = transform.localScale;
        ps = GetComponent<ParticleSystem>();
    }

    protected void Update()
    {
        targetScale = (isSelected) ? startingScale * 1.2f : startingScale;
        if (Util.GameManager.currentTurn != 0 || !GetComponent<Collider>().enabled)
        {
            targetScale = Vector2.zero;
        }

        if (!GetComponent<Collider>().enabled)
        {
            growSpeedX = 0.2f;
            growSpeedY = 0.3f;
        }

        scale.x = MathEx.Approach(scale.x, targetScale.x, growSpeedX);
        scale.y = MathEx.Approach(scale.y, targetScale.y, growSpeedY);

        if (!isSelected && resetTimer < 1f)
        {
            resetTimer += Time.deltaTime / GameInfo.Instance.cardResetTime;
            if (GetComponent<Collider>().enabled)
                transform.position = Vector3.Lerp(transform.position, startPosition, resetTimer);
        }

        transform.localScale = new Vector3(scale.x, scale.y, 1f);
        var pos = transform.position;
        pos.z = (isSelected) ? -5f : -2f;
        transform.position = pos;

        if (transform.localScale == new Vector3(0f, 0f, transform.localScale.z)) Destroy(gameObject);
    }

    protected void OnMouseDown()
    {
        if (!CanSelect) return;
        resetTimer = 0f;
        clickedPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        clickedPos.z = 0f;
        isSelected = true;
        offSet = transform.position - clickedPos;
    }

    protected void OnMouseDrag()
    {
        if (!CanSelect) { isSelected = false; return; }
        mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousepos.x, mousepos.y, 0) + offSet;
        isSelected = true;

        if (Mathf.Abs(startPosition.y - transform.position.y) > GameInfo.Instance.cardBreakDistance)
        {
            ps.Play();
        }
        else
        {
            ps.Stop();
        }
    }

    protected void OnMouseUp()
    {
        if (!CanSelect) return;
        isSelected = false;
        if (Mathf.Abs(startPosition.y - transform.position.y) > GameInfo.Instance.cardBreakDistance)
        {
            Activate();
            Util.GameManager.PlayCard();
            GetComponent<Collider>().enabled = false;
        }

        ps.Stop();
    }

    protected void Activate()
    {
        OnActivate.Invoke();
    }

    public void MovePlayerForward(int amount)
    {
        Util.Player.MoveForward(amount);
    }

    public void RotatePlayer(int amount)
    {
        Util.Player.Rotate(amount);
    }

    public void RotatePlayerToDirection(int direction)
    {
        Util.Player.Rotate(direction);
    }
}