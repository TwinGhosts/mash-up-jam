﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Compass : MonoBehaviour
{
    public Sprite compassNorth;
    public Sprite compassEast;
    public Sprite compassSouth;
    public Sprite compassWest;

    // Start is called before the first frame update
    private void Awake()
    {
        Util.Compass = this;
    }

    public void SetDirection(int dir)
    {
        if (dir == 0) GetComponent<Image>().sprite = compassNorth;
        if (dir == 1) GetComponent<Image>().sprite = compassEast;
        if (dir == 2) GetComponent<Image>().sprite = compassSouth;
        if (dir == 3) GetComponent<Image>().sprite = compassWest;
    }
}
